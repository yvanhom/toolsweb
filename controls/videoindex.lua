local vu = require("utils.video_utils")

local function videoindex(req)
    req.title = "Video | Tools of Yvan Hom"
    req.homeurl = "videoindex"
    req.hometitle = "Video"

    if not vu.videos then
        vu.update()
    end
    req.videolists = vu.videos
    return { render = true }
end

return videoindex