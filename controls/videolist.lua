local vu = require("utils.video_utils")

local function videoindex(req)
  req.title = "Video | Tools of Yvan Hom"
  req.homeurl = "videoindex"
  req.hometitle = "Video"

  if not vu.videos then
    vu.update()
  end

  req.listindex = tonumber(req.params.playlist)
  req.playlist = vu.videos[req.listindex]

  return { render = true }
end

return videoindex
