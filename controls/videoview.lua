local vu = require("utils.video_utils")

local function videoview(req)
  req.title = "View | Video | Tools of Yvan Hom"
  req.homeurl = "videoindex"
  req.hometitle = "Video"

  if not vu.videos then
    vu.update()
  end

  listindex = tonumber(req.params.playlist)
  videoindex = tonumber(req.params.index)

  req.video = vu.videos[listindex].videos[videoindex]

  return { render = true }
end

return videoview
