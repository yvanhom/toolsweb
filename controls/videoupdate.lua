local vu = require("utils.video_utils")

local function videoupdate(req)
    vu.update()

    return { redirect_to = req:url_for("videoindex") }
end

return videoupdate