local lapis = require("lapis")
local app = lapis.Application()

app:enable("etlua")
app.layout = require "views.layout"

app:get("index", "/", function(self)
  return require("controls.index")(self)
end)

app:get("videoindex", "/video", function(self)
  return require("controls.videoindex")(self)
end)

app:get("videoupdate", "/video/update", function(self)
  return require("controls.videoupdate")(self)
end)

app:get("videolist", "/video/list:playlist", function(self)
  return require("controls.videolist")(self)
end)

app:get("videoview", "/video/list:playlist/video:index", function(self)
  return require("controls.videoview")(self)
end)

return app
