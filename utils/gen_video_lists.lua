local plfile = require "pl.file"
local pldir = require "pl.dir"
local plpath = require "pl.path"
local json = require "cjson"

local infile = arg[1]
assert(infile, "Usage: lua/luajit gen_video_lists.lua infile")

local lists = json.decode(assert(plfile.read(infile)))

local support_exts = { 
    ["mp4"] = true, 
    ["webm"] = true 
}

for i, list in ipairs(lists) do
    assert(list.title and list.dir, "Wrong formatted input file.")

    list.basedir = plpath.basename(list.dir)
    list.videos = {}
    for i, file in ipairs(pldir.getfiles(list.dir)) do
        local ext = plpath.extension(file):sub(2)
        
        if ext and support_exts[ext] then
            filename = plpath.basename(file)
            len = #filename - #ext - 1
            local t = {}
            t.filename = filename
            t.path = list.basedir .. "/" .. filename
            if plpath.exists(string.format("%s/%s.en.vtt", list.dir, filename:sub(1, len))) then
                t.vttpath = string.format("%s/%s.en.vtt", list.basedir, filename:sub(1, len))
            end
            index = filename:match("^[0-9]*")
            t.ytkey = filename:sub(len - 10, len)
            t.title = filename:sub(#index + 2, len - 12)
            t.index = tonumber(index) or (#list.videos + 1)
            t.type = ext
            table.insert(list.videos, t)
        end
    end
    table.sort(list.videos, function(t1, t2)
        return t1.index < t2.index
    end)
end

print(json.encode(lists))

