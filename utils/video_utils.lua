local json = require 'cjson'
local config = require("lapis.config").get()

local t = {}

function t.update()
    local input = io.open(config.videos_path, "r")
    local str = input:read("*all")
    input:close()

    t.videos = json.decode(str)
end

return t